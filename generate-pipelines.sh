#!/bin/bash


# this fun little bashism fetches the latest synapse relases from github
# filters out the rc releases and takes the most recent 5
# then it munges it into a json list that we squirt into some yaml
# this yaml is picked up by gitlab as a pipeline defintion

# the end result: we can always have builds of the latest synapse versions
# without having to come in here and bump version strings manually

curl --retry 5 --silent "https://api.github.com/repos/element-hq/synapse/releases" > releases

cat releases \
| jq -r '.[] | select(.tag_name |contains ("rc")| not)|.tag_name' \
| head -n 5 \
| jq --raw-input --slurp --compact-output 'split("\n")[:-1] + ["latest"] | del(.[] | select(. == "v1.121.0"))' \
> latest-tags.json

latest_tags=$(cat latest-tags.json)


cat <<EOF
build-all:
  stage: build
  script:
    - docker build --no-cache --build-arg SYNAPSE_IMAGE=ghcr.io/element-hq/synapse:\$TAG -t \$IMAGE_NAME:\$TAG .
    - mkdir test-data
    - docker run -v $(pwd)/test-data:/data --entrypoint /test \$IMAGE_NAME:\$TAG
    - docker push \$IMAGE_NAME:\$TAG
  parallel:
    matrix:
      - TAG: $latest_tags
  only:
    - master
    - tags
EOF
