ARG SYNAPSE_IMAGE=ghcr.io/element-hq/synapse:latest
FROM $SYNAPSE_IMAGE
LABEL maintainer="Abel Luck <abel@guardianproject.info>"

COPY ./test /test
RUN set -ex ; \
  apt -y update ; \
  apt -y upgrade ; \
  apt -y install git python3-wheel python3-pip ; \
  pip3 install git+https://github.com/matrix-org/synapse-s3-storage-provider.git ; \
  apt -y remove git python3-pip python3-wheel ; \
  apt -y autoremove ; \
  apt clean

COPY ./entrypoint.sh /entrypoint.sh

ENTRYPOINT /entrypoint.sh
